<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ include file="./include.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<c:url value="/resources/styles/styles.css" />" rel="stylesheet" type="text/css" />
        <title>Welcome to Spring Web MVC project</title>
    </head>

    <body>
        <div class="Frame">
            <div class="banner">
                <div class="logo">
                    <img src="resources/images/logo.png"/>
                </div>
                <div class="logo-right"><img src="resources/images/banner.png"/></div>
            </div>
            <div class="clear"></div>
            <!--Menu -->
            <div class="menu">
                <ul class="menu">
                    <li><a href="Home.html">home</a></li>
                    <li><a href="Home.html">product management</a></li>
                    <li><a href="Home.html">provider management</a></li>
                    <li><a href="Home.html">type of product management</a></li>
                </ul>	
            </div>
            <div class="mainContent">
                <div class="slide">
                    <img src="resources/images/catalogeBanner.PNG"/>
                </div>

                <div class="content">
                    <div class="title">
                        <img src="resources/images/cow_sell_top.png"/>
                        <a>PROVIDER MANAGEMENT</a>
                    </div>
                    <div align="center">
                        <center>
                            <table border="0" width="90%">
                                <form:form action="add" modelAttribute="providerForm" method="POST" >
                                    <form:hidden path="IdProvider" />
                                    <tr>
                                        <td align="left" width="20%">Name of Country: </td>
                                        <td align="left" width="40%"><form:input path="NameOfCountry" size="30"/></td>
                                        <td align="left"><form:errors path="NameOfCountry" cssClass="error"/></td>
                                    </tr>
                                    <tr>
                                        <td><a href="<%=request.getContextPath()%>/provider" class="button">Back</a></td>
                                        <td align="center"><input type="submit" value="submit"/></td>
                                        <td></td>
                                    </tr>
                                </form:form>
                            </table>
                        </center>
                    </div>
                </div>
            </div>
            <div class="footer">
                <a>Alberomilk chấp nhận các loại thẻ</a>
                <div>
                    <ul class="menu_footer">
                        <li>
                            <a href="#"><img src="resources/images/bank_1.png"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/bank_2.png"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/bank_3.png"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/bank_4.png"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/bank_5.png"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/bank_10.png"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/bank_6.png"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/bank_8.png"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/bank_9.png"/></a>
                        </li>
                    </ul>
                </div>
                <div>
                    <ul class="menu_social">
                        <li>
                            <a href="#"><img src="resources/images/icon_facebook.png"/><a/>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/icon_flickr.png"/><a/>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/icon_google.png"/></a>
                        </li>
                        <li>
                            <a href="#"> <img src="resources/images/icons_youtube.png"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/icon_pinterest.png"/></a>
                        </li>
                        <li>
                            <a href="#"><img src="resources/images/icon_twitter.png"/></a>
                        </li>
                    </ul>
                </div>
                <p>Design by: Tran Thi Duc Lin</p>
            </div>
        </div>
    </body>
</html>
