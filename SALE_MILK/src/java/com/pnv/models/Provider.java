package com.pnv.models;
// Generated May 13, 2015 8:04:12 AM by Hibernate Tools 3.2.1.GA


import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Provider generated by hbm2java
 */
@Entity
@Table(name="provider"
    ,catalog="milkshopbaby"
)
public class Provider  implements java.io.Serializable {


     private Integer idProvider;
     private String nameOfCountry;
     private List<Product> products;

    public Provider() {
    }

	
    public Provider(String nameOfCountry) {
        this.nameOfCountry = nameOfCountry;
    }
    public Provider(String nameOfCountry, List<Product> products) {
       this.nameOfCountry = nameOfCountry;
       this.products = products;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)
    
    @Column(name="IdProvider", unique=true, nullable=false)
    public Integer getIdProvider() {
        return this.idProvider;
    }
    
    public void setIdProvider(Integer idProvider) {
        this.idProvider = idProvider;
    }
    
    @Column(name="NameOfCountry", nullable=false, length=50)
    public String getNameOfCountry() {
        return this.nameOfCountry;
    }
    
    public void setNameOfCountry(String nameOfCountry) {
        this.nameOfCountry = nameOfCountry;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="provider")

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
    




}


