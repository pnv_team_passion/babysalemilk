/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api.controllers;

import com.pnv.dao.productdao;
import com.pnv.dao.typeOfProductDao;
import com.pnv.models.Product;
import com.pnv.models.Typeproduct;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author linttd
 */
@Controller
@RequestMapping(value = "api/index")
public class productApiController {
     @Autowired
    private productdao productdao;
     private typeOfProductDao typeProduct;
     
     @RequestMapping(method = RequestMethod.GET)
    public String viewProductPage(ModelMap map) {
         List<Product> product_list = productdao.findAll();
         map.put("product_list", product_list);
         List<Typeproduct> typeproduct_list = typeProduct.findAll();
         map.put("typeproduct_list", typeproduct_list);
        return "index";
     }
     
     @RequestMapping(value = "{IdProduct}", method = RequestMethod.GET)
    public  @ResponseBody Product findByproductId(@PathVariable(value = "IdProduct") Integer IdProduct) {
        
//        Employees emp = new Employees();
//        emp.setEmpName("ddd");
//        emp.setAddress("ssss");
//        List<Employees> emp_lst = new ArrayList<Employees>();
        Product pro = productdao.findByproductId(IdProduct);
       // dep.setEmployees(emp_lst);
        return productdao.findByproductId(IdProduct);
    }
     @RequestMapping(value = "{IdTypeProduct}", method = RequestMethod.GET)
    public  @ResponseBody Typeproduct findByTypeProductId(@PathVariable(value = "IdTypeProduct") Integer IdTypeProduct) {

         Typeproduct type= typeProduct.findByTypeProductId(IdTypeProduct);
       // dep.setEmployees(emp_lst);
        return typeProduct.findByTypeProductId(IdTypeProduct);
     }
}
