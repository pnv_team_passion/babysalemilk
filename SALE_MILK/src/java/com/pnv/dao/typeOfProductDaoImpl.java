/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Typeproduct;
import com.pnv.utils.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;

/**
 *
 * @author linttd
 */
@Service
public class typeOfProductDaoImpl implements typeOfProductDao{
    private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    @Override
    public void saveOrUpdate(Typeproduct typeproduct) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.saveOrUpdate(typeproduct);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }

   

    @Override
    public void delete(Typeproduct typeproduct) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(typeproduct);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }

    @Override
    public List<Typeproduct> findAll() {
        Session session = sessionFactory.openSession();
        List<Typeproduct> departmentsList = session.createQuery("from Typeproduct").list();
        session.close();
        return departmentsList;
    }

    @Override
    public Typeproduct findByTypeProductId(int IdTypeProduct) {
        Typeproduct department = null;
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            department = (Typeproduct) session.get(Typeproduct.class, IdTypeProduct);
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        return department;
    }

    @Override
    public Typeproduct findByTypeProductCode(String departmentsCode) {
        Typeproduct typeproduct = null;
        String strQuery = "from typeproduct WHERE TypeProduct = :TypeProduct ";
        Session session = sessionFactory.openSession();
        Query query = session.createQuery(strQuery);
        query.setParameter("TypeProduct", departmentsCode);
        typeproduct = (Typeproduct) query.uniqueResult();
        session.close();
        return typeproduct;
    }

}
