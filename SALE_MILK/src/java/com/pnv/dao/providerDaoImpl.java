/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Provider;
import com.pnv.utils.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;

/**
 *
 * @author linttd
 */
@Service
public class providerDaoImpl implements providerDao{
    private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    @Override
    public void saveOrUpdate(Provider provider) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.saveOrUpdate(provider);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }

   

    @Override
    public void delete(Provider provider) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(provider);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }

    @Override
    public List<Provider> findAll() {
        Session session = sessionFactory.openSession();
        List<Provider> providerList = session.createQuery("from Provider").list();
        session.close();
        return providerList;
    }

    @Override
    public Provider findByProviderId(int IdProvider) {
        Provider provider = null;
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            provider = (Provider) session.get(Provider.class, IdProvider);
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        return provider;
    }

    @Override
    public Provider findByProviderCode(String NameOfCountry) {
        Provider provider = null;
        String strQuery = "from provider WHERE NameOfCountry = :NameOfCountry ";
        Session session = sessionFactory.openSession();
        Query query = session.createQuery(strQuery);
        query.setParameter("NameOfCountry", NameOfCountry);
        provider = (Provider) query.uniqueResult();
        session.close();
        return provider;
    }

}
