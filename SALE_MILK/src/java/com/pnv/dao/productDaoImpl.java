/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Product;
import com.pnv.models.Provider;
import com.pnv.utils.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;

/**
 *
 * @author linttd
 */
@Service
public class productDaoImpl implements productdao{
private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    @Override
    public void saveOrUpdate(Product product) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            
            session.saveOrUpdate( product );
            transaction.commit();
            
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }
    
     @Override
    public void delete(Product product) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try {
            session.delete(product);
            transaction.commit();
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
    }
     
     @Override
    public List<Product> findAll() {
        Session session = sessionFactory.openSession();
        List<Product> proList = session.createQuery("from Product").list();
        session.close();
        return proList;
    }
     @Override
    public Product findByproductId(int IdProduct) {
        Product product = null;
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            product = (Product) session.get(Provider.class, IdProduct);
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        return product;
    }
@Override
    public List<Product> findByproductName(String employeeName) {
       Provider provider = null;
        String strQuery = "from product WHERE Name LIKE :Name ";
        Session session = sessionFactory.openSession();
        Query query = session.createQuery(strQuery);
        query.setParameter( "Name", "%" + employeeName + "%" );
        List<Product> proList = query.list();
        session.close();
        return proList;
    }

    
    
}
