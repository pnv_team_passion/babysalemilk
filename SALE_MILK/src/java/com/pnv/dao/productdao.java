/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Product;
import java.util.List;

/**
 *
 * @author linttd
 */
public interface productdao {
    public void saveOrUpdate(Product product);

    public void delete(Product product);

    public List<Product> findAll();

    public Product findByproductId(int IdProduct);

    public List<Product> findByproductName(String Name);
}
