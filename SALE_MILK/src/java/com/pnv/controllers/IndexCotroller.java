/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.productdao;
import com.pnv.dao.typeOfProductDao;
import com.pnv.models.Product;
import com.pnv.models.Typeproduct;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author linttd
 */@Controller

public class IndexCotroller {
     @Autowired
     private productdao pro;
     
     private typeOfProductDao type;
     
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String viewWelcomePage(ModelMap map) {
        
         List<Product> product_list = pro.findAll();
         map.put("product_list", product_list);   
//         List<Typeproduct> typeproduct_list = type.findAll();
//         map.put("typeproduct_list", typeproduct_list);
        return "index";
    }
    
    
}
