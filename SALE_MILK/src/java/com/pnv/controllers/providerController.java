/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controllers;

import com.pnv.dao.providerDaoImpl;
import com.pnv.models.Provider;
import com.pnv.utils.Constant;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author linttd
 */
@Controller
@RequestMapping(value = "/provider")
public class providerController {
    @Autowired
    private providerDaoImpl provider_Dao = new providerDaoImpl();

    public providerController() {
    }
    @RequestMapping(method = RequestMethod.GET)
    public String viewproviderPage(ModelMap map) {
        /**
         * Get all titles
         */
        List<Provider> provider_list = provider_Dao.findAll();
        map.put("provider_list", provider_list);
        return "provider";
    }
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String viewEditTitlePage(@RequestParam(value = "IdProvider", required = true) int IdProvider, ModelMap map) {

        Provider providerForm = provider_Dao.findByProviderId(IdProvider);
        map.addAttribute("providerForm", providerForm);
        return "provider_add";
    }
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {

        Provider providerForm = new Provider();
        map.addAttribute("providerForm", providerForm);
        return "provider_add";

    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("providerForm") Provider providerForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("providerForm", providerForm);
            return "provider_add";
        }
        provider_Dao.saveOrUpdate(providerForm);

        /**
         * Get all titles
         */
        List<Provider> provider_list = provider_Dao.findAll();
        map.put("provider_list", provider_list);
        map.addAttribute("add_success", "ok");

        return "provider";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteTitle(@RequestParam(value = "IdProvider", required = true) int IdProvider, ModelMap map) {

        provider_Dao.delete(provider_Dao.findByProviderId(IdProvider));

        return Constant.REDIRECT + "/provider";

    }
}
